import { expect } from 'chai';
import mlog from 'mocha-logger';
import { Maybe, Nothing } from '../src/index';

describe('Maybe', () => {

  context('#fromValue', () => {
    it('return nothing if item is null', () => {
      const data = Maybe.fromValue(null);
      expect(data.isNothing()).to.eq(true);
    });
    it('return Just(value) if items is not null', () => {
      const data = Maybe.fromValue('value');
      mlog.log(`Maybe.fromValue('value') === ${data}`);
      expect(data.isNothing()).to.eq(false);
    });
  });
});

describe('Just', () => {
  context('.map', () => {
    it('return Just(result) if result is not undefined or null', () => {
      const data = Maybe.fromValue('value').map((s: string) => s[0]);
      expect(data.isNothing()).to.eq(false);
    });
    it('return Nothing if result is undefined or null', () => {
      const data = Maybe.fromValue('value').map((s: string) => s[9]);
      expect(data.isNothing()).to.eq(true);
    });
  });

  context('.value', () => {
    it('return result', () => {
      const data = Maybe.fromValue('value').value('fuck');
      expect(data).to.eq('value');
    });
  });
});

describe('Nothing', () => {
  context('.map', () => {
    it('always return nothing', () => {
      const data = Nothing.instance() as Nothing<number>;
      const result = data.map((n: number) => n === 0 ? undefined : 3).map((n: number) => 3);
      expect(result.isNothing()).to.eq(true);
    });
  });
  context('.value', () => {
    it('return the default value given', () => {
      const data = Maybe.fromValue('0').map((x: string) => x[1]);
      expect(data.value('3')).to.eq('3');
    });
    it('return undefined if no default value were given', () => {
      const data = Maybe.fromValue('0').map((x: string) => x[1]);
      expect(data.value()).to.eq(undefined);
    });
  });
});
