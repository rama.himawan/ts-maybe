# ts-maybe
v0.0.x is designed for nodejs8 (?)
v1.0.x is designed for nodejs10+
typescript implementation of Maybe monad.
type-safe-ly handling nullable value

```javascript
Maybe.fromValue('Something')
>>> Just { _value: 'Something' }

Maybe.fromValue('Something').map(x => x[0])
>>> Just { _value: 'S' }

Maybe.fromValue('Something').map(x => x[99]);
>>> Nothing {};

Maybe.fromValue('Something').map(x => x[99]).map(x => x[999])
>>> Nothing {}

```