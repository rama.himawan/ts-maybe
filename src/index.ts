class Just<T> {

  constructor(private readonly v: T) {}

  /**
   * backward compatibility;
   */
  get _value(): T {
    return this.v;
  }

  flatMap<T2>(f: (a: T) => Optional<T2>): Optional<T2> {
    return f(this._value);
  }

  isNothing(): false {
    return false;
  }

  map<T2>(f: (a: T) => T2 | undefined | null): Optional<T2> {
    const result = f(this._value);
    if (result === null || result === undefined) { return Maybe.fromValue(); }
    return Maybe.fromValue(result);
  }

  toString(): string {
    return `Just(${this.value()})`;
  }

  value<T2 extends undefined | T>(t?: T2): T2 {
    return this._value as T2;
  }

  /**
   * @description
   * fake attribute
   * a compile time only helper.
   */
  get __value(): T { throw new Error('__value is a compile-time only helper'); }

}

export class Nothing<T> {
  static instance<T>(): Nothing<T> {
    return Nothing.INSTANCE as Nothing<T>;
  }

  static INSTANCE: Nothing<any> = new Nothing();

  v: undefined = undefined;

  flatMap<T2>(f: (a: T) => Optional<T2>): Optional<T2> {
    return Nothing.instance<T2>();
  }

  isNothing(): true {
    return true;
  }

  map<T2>(f: (a: T) => T2 | undefined | null): Optional<T2> {
    return Nothing.instance<T2>();
  }

  toString(): string {
    return 'Nothing';
  }

  /**
   * @description
   * fake attribute
   * a compile time only helper.
   */
  get __value(): T { throw new Error('__value is a compile-time only helper'); }

  value<T2 extends undefined | T>(t?: T2): T2 {
    return t as T2;
  }

}

export type Optional<T> = Just<T> | Nothing<T>;
export class Maybe<T> extends Just<T> {
  // tslint:disable-next-line:typedef
  static YIELD = <T>(value?: T) => {};

  static of<T>(value?: T): Optional<T> {
    return this.fromValue(value);
  }

  static fromObject<O extends { [k: string]: Optional<any>}>(obj: O): { [k in keyof O]: O[k]['__value'] } {
    const keys: Array<(keyof O)> = Object.keys(obj);
    const accumulator: Partial<{ [k in keyof O]: O[k]['__value'] }> = {};
    for (const key of keys) {
      accumulator[key] = obj[key].value();
    }
    return accumulator as { [k in keyof O]: O[k]['__value'] };
  }

  static fromValue<T>(value?: T): Optional<T> {
    if (value === undefined) { return Nothing.instance() as Optional<T>; }
    if (value === null) { return Nothing.instance() as Optional<T>; }
    return new Just(value);
  }

}

function test(): void {
  const a = Maybe.of(5 as undefined | number).value();
}
